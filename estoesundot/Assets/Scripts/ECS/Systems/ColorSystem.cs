using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.VisualScripting;

internal partial struct ColorSystem : ISystem
{
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<URPMaterialPropertyBaseColor>();
        state.RequireForUpdate<ColorData>();
    }

    public void OnUpdate(ref SystemState state)
    {
        EntityManager em = state.EntityManager;
        float deltaTime = SystemAPI.Time.DeltaTime;

        foreach (var (compColor, compMaterial, dot) in SystemAPI.Query<RefRW<ColorData>, RefRW<URPMaterialPropertyBaseColor>>()
            .WithEntityAccess())
        {
            compColor.ValueRW.elapsed -= deltaTime;
            if (compColor.ValueRO.elapsed <= 0)
            {
                compColor.ValueRW.elapsed += compColor.ValueRO.interval;

                //UnityEngine.Debug.Log("Color changed to " + UnityEngine.Random.Range(0, 255) + " " + UnityEngine.Random.Range(0, 255));
                //UnityEngine.Debug.Log("Color changed to " + compColor.random.NextInt(255) + " " + compColor.random.NextInt(255));
                float4 color = compColor.ValueRW.random.NextFloat4(0, 1);
                color.w = 1;
                compMaterial.ValueRW.Value = color;
            }
        }
    }
}