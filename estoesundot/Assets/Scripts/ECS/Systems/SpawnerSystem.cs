using Unity.Burst;
using Unity.Entities;

[BurstCompile]
internal partial struct SpawnerSystem : ISystem
{
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<Spawner>();
    }

    public void OnUpdate(ref SystemState state)
    {
        float deltaTime = SystemAPI.Time.DeltaTime;
        foreach (var spawner in SystemAPI.Query<SpawnerAspect>())
        {
            spawner.ElapseTime(deltaTime, state.EntityManager, SystemAPI.GetComponent<ColorData>(spawner.Prefab));
        }
    }
}