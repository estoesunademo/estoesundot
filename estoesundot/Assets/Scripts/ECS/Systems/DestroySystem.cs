using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;

internal partial struct DestroySystem : ISystem
{
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<ColorData>();
    }

    public void OnUpdate(ref SystemState state)
    {
        if (ColoPicker.Instance == null) return;

        if (!Input.GetKeyDown(KeyCode.Space))
            return;

        float4 color = ColoPicker.Instance.Color;

        var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        EntityCommandBuffer ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

        foreach (var (compColor, compMaterial, dot) in SystemAPI.Query<RefRO<ColorData>, RefRO<URPMaterialPropertyBaseColor>>()
            .WithEntityAccess())
        {
            float4 dot_col = compMaterial.ValueRO.Value;

            if (dot_col.x - .1f > color.x) continue;
            if (dot_col.x + .1f < color.x) continue;
            if (dot_col.y - .1f > color.y) continue;
            if (dot_col.y + .1f < color.y) continue;
            if (dot_col.z - .1f > color.z) continue;
            if (dot_col.z + .1f < color.z) continue;
            Debug.Log(dot_col);

            ecb.DestroyEntity(dot);
        }
    }
}