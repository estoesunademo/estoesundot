using System;
using Unity.Entities;
using Unity.Rendering;

internal partial struct ColorArraySystem : ISystem
{
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<URPMaterialPropertyBaseColor>();
        state.RequireForUpdate<ColorArray>();
    }


    public void OnUpdate(ref SystemState state)
    {
        EntityManager em = state.EntityManager;
        float deltaTime = SystemAPI.Time.DeltaTime;

        foreach (var (comp, dot) in SystemAPI.Query<ColorArray>().WithEntityAccess())
        {
            float elapsed = comp.elapsed;
            elapsed -= deltaTime;
            int index = comp.index;
            if (elapsed <= 0)
            {
                elapsed += comp.interval;
                index = (index + 1) % comp.colors.Length;
                em.SetComponentData(dot, new URPMaterialPropertyBaseColor
                {
                    Value = comp.colors[index]
                });
            }

            em.SetComponentData(dot, new ColorArray
            {
                colors = comp.colors,
                interval = comp.interval,
                elapsed = elapsed,
                index = index
            });
        }
    }
}