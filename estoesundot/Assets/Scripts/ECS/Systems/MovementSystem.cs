using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

internal partial struct MovementSystem : ISystem
{
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<Movement>();
    }

    public void OnUpdate(ref SystemState state)
    {
        MiPrimmerRunnableJob movement = new MiPrimmerRunnableJob
        {
            deltaTime = SystemAPI.Time.DeltaTime
        };
        movement.ScheduleParallel();
    }
}

internal partial struct MiPrimmerRunnableJob : IJobEntity
{
    public float deltaTime;
    private void Execute(ref LocalTransform transform, in Movement movement)
    {
        float3 direction = movement.direction;
        transform.Position += new float3(direction.x, direction.y, direction.z) * movement.speed * deltaTime;
    }
}