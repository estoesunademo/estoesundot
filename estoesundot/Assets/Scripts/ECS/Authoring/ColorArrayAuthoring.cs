using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;

public class ColorArrayAuthoring : MonoBehaviour
{
    [SerializeField] private Color[] colors;

    private class ColorArrayBaker : Baker<ColorArrayAuthoring>
    {
        public override void Bake(ColorArrayAuthoring authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);

            NativeArray<float4> cols = new NativeArray<float4>(authoring.colors.Length, Allocator.Persistent);
            for (int i = 0; i < authoring.colors.Length; i++)
            {
                cols[i] = new float4(authoring.colors[i].r, authoring.colors[i].g, authoring.colors[i].b,
                    authoring.colors[i].a);
            }
            
            AddComponent(entity, new ColorArray
            {
                colors = cols,
            });
            AddComponent(entity, new URPMaterialPropertyBaseColor
            {
                Value = new Vector4(authoring.colors[0].r, authoring.colors[0].g, authoring.colors[0].b,
                    authoring.colors[0].a)
            });
        }
    }
}

public struct ColorArray : IComponentData
{
    public NativeArray<float4> colors;
    public float interval;
    public float elapsed;
    public int index;
}