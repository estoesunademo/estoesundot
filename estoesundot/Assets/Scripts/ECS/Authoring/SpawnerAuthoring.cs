using Unity.Entities;
using UnityEngine;

internal class SpawnerAuthoring : MonoBehaviour
{
    public GameObject prefab;
    public float spawnRateSec;

    private class SpawnerBaker : Baker<SpawnerAuthoring>
    {
        public override void Bake(SpawnerAuthoring authoring)
        {
            var entity = GetEntity(TransformUsageFlags.None);
            AddComponent(entity, new Spawner
            {
                entityPrefab = GetEntity(authoring.prefab, TransformUsageFlags.Dynamic),
                spawnRateSec = authoring.spawnRateSec,
                elapsedTime = authoring.spawnRateSec,
                random = new Unity.Mathematics.Random((ushort)Random.Range(1, 65536))
            });
        }
    }
}

public struct Spawner : IComponentData
{
    public Entity entityPrefab;
    public float spawnRateSec;    //Com ara tenim sistemes i les entitats guarden les dades,
    //el temps d'spawn ser� una dada a desar al seu component
    public float elapsedTime;
    public Unity.Mathematics.Random random;
}