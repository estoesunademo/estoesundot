using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;
using Random = UnityEngine.Random;

internal class ColorAuthoring : MonoBehaviour
{
    [SerializeField] private float interval;
    [SerializeField] private Color color;

    private class ColorBaker : Baker<ColorAuthoring>
    {
        public override void Bake(ColorAuthoring authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent(entity, new URPMaterialPropertyBaseColor
            {
                Value = new Vector4(authoring.color.r, authoring.color.g, authoring.color.b, authoring.color.a),
            });
            AddComponent(entity, new ColorData
            {
                interval = authoring.interval,
                elapsed = authoring.interval,
                random = new Unity.Mathematics.Random((ushort)Random.Range(1, 65536)),
                color = new int4(new Vector4(authoring.color.r, authoring.color.g, authoring.color.b,
                    authoring.color.a))
            });
        }
    }
}

public struct ColorData : IComponentData
{
    public float interval;
    public float elapsed;
    public int4 color;
    public Unity.Mathematics.Random random;
}