using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

internal class MovementAuthoring : MonoBehaviour
{
    public float speed;
    public Vector3 direction;

    private class MovementBaker : Baker<MovementAuthoring>
    {
        public override void Bake(MovementAuthoring authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent(entity, new Movement
            {
                speed = authoring.speed,
                direction = authoring.direction.normalized
            });
        }
    }
}

public struct Movement : IComponentData
{
    public float speed;
    public float3 direction;
}