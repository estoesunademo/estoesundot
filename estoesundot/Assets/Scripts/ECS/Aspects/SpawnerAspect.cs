using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEditor.PackageManager;

// Equivalent a PREFAB (creemos : Posiblemente)
internal readonly partial struct SpawnerAspect : IAspect
{
    private readonly RefRW<Spawner> _spawner;
    public Entity Prefab => _spawner.ValueRO.entityPrefab;

    public void ElapseTime(float deltaTime, EntityManager em, ColorData data)
    {
        _spawner.ValueRW.elapsedTime -= deltaTime;
        if (!(_spawner.ValueRO.elapsedTime <= 0)) return;
        _spawner.ValueRW.elapsedTime += _spawner.ValueRO.spawnRateSec;
        Spawn(em, data);
    }

    private void Spawn(EntityManager em, ColorData data)
    {
        var entity = em.Instantiate(Prefab);
        float3 direction = new float3 { x = 0f, y = 1f, z = 0f };
        float3 position = _spawner.ValueRW.random.NextFloat3(-10, 10);
        position.y = 0;
        const float speed = 1;

        
        em.SetComponentData(entity, new ColorData
        {
            interval = data.interval,
            elapsed = data.elapsed,
            random = new Unity.Mathematics.Random(_spawner.ValueRW.random.NextUInt(1,65536)),
            color = data.color
        });
        
        em.SetComponentData(entity, new Movement
        {
            speed = speed,
            direction = direction
        });

        em.SetComponentData(entity, new LocalTransform
        {
            Position = position,
            Scale = 1
        });

    }

}
