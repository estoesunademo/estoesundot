using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEditor;
using UnityEditor.Build.Pipeline;
using UnityEngine;
using UnityEngine.UI;

public class ColoPicker : MonoBehaviour
{
    [SerializeField] private Slider SliderR;
    [SerializeField] private Slider SliderG;
    [SerializeField] private Slider SliderB;

    public float4 Color => new float4(SliderR.value, SliderG.value, SliderB.value, 1);

    public static ColoPicker Instance { get; internal set; }

    private void Awake()
    {
        Instance = this;
    }
}
